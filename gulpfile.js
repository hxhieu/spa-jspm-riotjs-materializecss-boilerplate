var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');

gulp.task('build:sass', function () {
    gulp.src('./src/sass/app.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./'));
});

gulp.task('watch:sass', function () {
    gulp.watch(['./src/sass/**/*.scss'], ['build:sass']);
});

gulp.task('build:copy', function () {
    gulp.src([
        './app.css',
        './font/**/*'],
        {
            base: './'
        }).pipe(gulp.dest('./build/'));

    gulp.src('./index_production.html')
      .pipe(rename('index.html'))
      .pipe(gulp.dest('./build/'));
});

gulp.task('build', ['build:sass', 'build:copy']);
gulp.task('default', ['build:sass', 'watch:sass']);
